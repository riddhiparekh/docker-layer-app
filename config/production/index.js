class Config {
    constructor(){
    }
    getPort(){
        return process.env.port? process.env.port: 3000
    }

    getTokenExpirationTime(){
        return 3600;
    }
}

class DbConfig {
    //TODO: Please add proper credentials here
    getUser(){ return 'evivveuser' }
    getHost(){ return 'evivve-test.crek2irth1dq.ap-south-1.rds.amazonaws.com' } 
    getDatabase(){ return 'evivve' }
    getPassword(){ return 'evivve12345'  } 
    getPort(){ return 3306 }

}

let config = new Config();
let dbConfig = new DbConfig();

module.exports = {
    serverConfig: config,
    dbConfig: dbConfig
}