// Import Moduels
const Docker = require('dockerode');

/* Connect docker by socket file path*/
const docker = new Docker({ socketPath: '/var/run/docker.sock' });

/* Create an docker Container */
exports.runContainer = function (param) {
    return new Promise(((resolve, reject) => {
        docker.createContainer(param, (error, container) => {
            if (error) {
                console.log('err in creating container');
                reject(error);
            } else {
                container.start((err, data) => {
                        if (err) {
                            console.log('err in start container');
                            console.log(err);
                            reject(err);
                        }
                        console.log('Container created successfully');
                        resolve(container.id);
                });
            }
        });
    }));
};

/* remove container by id  */
exports.removeContainer = function (id) {
    return new Promise(((resolve, reject) => {
        console.log('Container Remove Started');

         this
            .inspectContainer(id)
            .then(async (result) => {

                if (result.State.Status == "running") {

                    return this.killContainer(id);
               
                }
                else if(result.State.Status=="exited")
                {
                    return true;
                }
                else
                {
                    return result;
                }


             }).then((fromResolve) => {
               
                     console.log('Delete Container');                
                    return this.deleteContainer(id);
            
             }).then((fromResolve) => {
                resolve(true);
            })
            .catch((err) => {
                console.log('Remove Container Satus ' + err.message);
                resolve(null);
            });

      
    }));
};

// restart an container
exports.restartContainer = function (id) {
    return new Promise(((resolve, reject) => {

         console.log('Container Restart Started');        
         this
            .inspectContainer(id)
            .then(async (result) => {

                if (result.State.Status == "running") {

                    return await this.killContainer(id);
               
                }
                else if(result.State.Status=="exited")
                {
                    return await true;
                }
                else
                {
                    return await result;
                }
      
             }).then(async (fromResolve) => {

                return await this.startContainer(id);

             }).then(async (fromResolve) => {
             
                 return await this.inspectContainer(id);
             
             }).then((fromResolve) => {

                  console.log('Container Status : '+fromResolve.State.Status)
                    resolve(true);
            })
            .catch((err) => {
                console.log('Err in container restart')
                console.log(err);
                reject(err);
            });


    }));
};

/* get a container by id  */
exports.getContainer = function (id) {
    return new Promise(((resolve, reject) => {
            docker.getContainer(id, (err, data) => {
                    if (err) {
                        reject(err);
                    }
                    resolve(data);
        });
     }));
};

/* remove a container by id  */
exports.deleteContainer = function (id) {
    return new Promise(((resolve, reject) => {
            const container = docker.getContainer(id);

            container.remove((err, data) => {
                    if (err) {
                        resolve(null);
                    }
                    resolve(data);
        });
     }));
};

/* List all containers  */
exports.listContainers = function (id) {
     return new Promise(((resolve, reject) => {
       
       
       docker.listContainers({all:true},function (err, containers) {
                    if (err) {
                        console.log(err);
                        reject(err);
                    }
                    resolve(containers);
        });

     }));
};
/* Start  a container by id  */
exports.startContainer = function (id) {
     return new Promise(((resolve, reject) => {
        const container = docker.getContainer(id);

            container.start((err, data) => {
                    if (err) {
                        reject(err);
                    }
                    resolve(data);
        });
     }));
};

/* Kill a container by id  */
exports.killContainer = function (id) {
     return new Promise(((resolve, reject) => {
        const container = docker.getContainer(id);

            container.kill((err, data) => {
                    if (err) {
                        reject(err);
                    }

                    resolve(data);
        });
     }));
};

/* Stop a container by id  */
exports.stopContainer = function (id) {
	 return new Promise(((resolve, reject) => {
        const container = docker.getContainer(id);

    		container.stop((err, data) => {
        			if (err) {
        				reject(err);
        			}

        			resolve(data);
        });
	 }));
};

// Stop all running container
exports.stopAllContainer = function (id) {
	 return new Promise(((resolve, reject) => {
	 	docker.listContainers((err, containers) => {
	 		containers.forEach((containerInfo) => {
	 			docker.getContainer(containerInfo.Id).stop(cb);
	 		});
	 	});
	 }));
};

// Inspect an docker app by id
exports.inspectContainer = function (id) {
	 return new Promise(((resolve, reject) => {
	 		const container = docker.getContainer(id);

    		container.inspect((err, data) => {
        			if (err) {
        				reject(err);
        			}
        			resolve(data);
        });
	 }));
};
