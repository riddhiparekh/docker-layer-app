/* Import Modules */
const https = require("https"),
      fs = require("fs"),
      express = require("express"),
      helmet = require("helmet");

const Components = require('./components/');
const Middlewares = require('./middlewares');

class Server {
    constructor(config) {
        this.app = express();
        this.app.use(helmet()); // Add Helmet as a middleware
        this.config = config;
        this.initializeApp();
    }

    initializeApp() {
        this.initConfig();
        this.setMiddlewares();
        this.setComponents();
    }

    initConfig() {
        this.port = this.config.serverConfig.getPort();
        this.app.set('port', this.port);
    }

    setMiddlewares() {
        this.middlewares = new Middlewares(this.app);
    }

    setComponents() {
        this.components = new Components(this.app);
    }

    startServer() {
        // Init a Key Files
        const options = {
              key: fs.readFileSync("/etc/letsencrypt/live/api.evivve.com/privkey.pem"),
              cert: fs.readFileSync("/etc/letsencrypt/live/api.evivve.com/fullchain.pem"),
        };
        // Create a  Server Request with HTTPS  
        https.createServer(options, this.app).listen(this.app.get('port'), () => {
            console.log(`Server is running on port : ${this.port}`); // eslint-disable-line no-console
        });
        // Normal Application with HTTP
        this.app.listen(3030);

        this.isServerUp();
    }

    isServerUp() {
        
        this.app.get('/status', (req, res) => {
            res.send('Docker layer deploy version 2.0');
        });
    }
}

module.exports = Server;
