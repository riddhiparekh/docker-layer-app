/* eslint-disable no-underscore-dangle */
// Import Modules
const application = require('./application.model');
const responseService = require('../../utils/ResponseService');
const dockerService = require('../../services/docker');
const logger = require('../../utils/logging');
const helper = require('./application.helper');
const moment = require('moment');

 /* Logout an application */

exports.logoutApp = function (req, res) {

    console.log('logout an App:' + req.body.code);
    let container_id,user_id = null;
   
    application
        .getAppByCode(req.body.code)
        .then(async (result) => {

            if (result) {
                container_id = result['st_container'];
                user_id = result['in_user_id'];
                console.log('Container Remove : ' + container_id);
                return await dockerService.removeContainer(container_id);
            }
            else {
                return await false;
            }

        }).then((result) => {

            console.log(result);
            console.log('Remove Application Entry from Database');
            if(user_id!==null)
            {
                return application.deleteSpecificApp(user_id)
            }
            else
            {
                return true;
            }

        }).then((result) => {

      
            if (result) {
                responseService.send({
                    status: responseService.getCode().codes.OK,
                    data: "true",
                }, res);

            }
            else {
                responseService.send({
                    status: responseService.getCode().codes.FAILURE,
                    data: "Internal Server Error",
                }, res);
            }

        }).catch((err) => {

            console.log(err.message);
            responseService.send({
                status: responseService.getCode().codes.FAILURE,
                data: err.message,
            }, res);

        });


};
// Restart an app by creating a new docker app
exports.restartApp = function (req, res) {

    console.log('restart an App: ' + req.body.code);
    let container_id = null;
    let data ={};

     application
        .getAppByCode(req.body.code)
        .then(async (result) => {

            if (result) {
                data=result;
                console.log('Container Remove : ' + data['st_container']);
                return await dockerService.removeContainer(data['st_container']);
            }
            else {
                return false;
            }

        }).then(async (result) => {

            return await helper.getAppPort(data['in_port']);

        }).then(async (port) => {

            console.log('Start New Container with port');
            data['in_port'] = port;
            return await this.createNewApp(data['in_user_id'],port); 

        }).then(async (result) => {

            data = result;
            
            console.log('inspect Container:'+data['st_container']);

            return await dockerService.inspectContainer(data['st_container']);

        }).then((result) => {
            
             if (result.State.Status == "running") 
             {
               
                    responseService.send({
                        status: responseService.getCode().codes.OK,
                        data: data,
                    }, res);

             }
             else
             {
                     responseService.send({
                        status: responseService.getCode().codes.FAILURE,
                        data: "An App Restarting Failed.Please Try again.",
                    }, res); 
             }

       

        }).catch((err) => {

            console.log(err.message);
            responseService.send({
                status: responseService.getCode().codes.FAILURE,
                data: 'Internal Server Error',
            }, res);

        });
};
// get an app info by code
exports.getAppInfo = function (req, res) {


    console.log('Get an App Info=' + req.body.code);
    application
        .getAppByCode(req.body.code)
        .then((result) => {


            responseService.send({
                status: responseService.getCode().codes.OK,
                data: result,
            }, res);

        }).catch((err) => {

            responseService.send({
                status: responseService.getCode().codes.INVALID_HEADERS,
                data: err,
            }, res);

        });


};
 
// Get an application status and return new available port
exports.checkAppStatus = function (id) {
 
    console.log(' checking an application status ');

    let finalResp = {};
    let container_id = null;
    return new Promise(((resolve, reject) => {

        application
            .checkAppStatus(id)
            .then(async (result) => {

                console.log(result);

                if (result !== null) {
                    // If port exists ,then get container ID
                    container_id = result['st_container'];
                    return await helper.getAppPort(result['in_port']);
                }
                else {
                    // If port does not exists, then check new port
                    return await helper.getAppPort(3000);
                }

            }).then(async (fromResolve) => {

                     finalResp['new_port'] = fromResolve;
                    if(container_id===null)
                    {
                        return true;
                    }
                    else
                    {
                       // Remove a container
                       console.log('Process for remove a container')
                       return await dockerService.removeContainer(finalResp['st_container']);  
                    }
                   
            }).then((fromResolve) => {

               console.log('Again check for delete container')
                if(fromResolve===null)
                {
                    console.log('Delete docker APP')
                    return dockerService.deleteContainer(finalResp['st_container']);
                }
                else
                {
                    return fromResolve;
                }

            }).then((fromResolve) => {
                console.log('Final Respose from App status')
                resolve(finalResp);

            })
            .catch((err) => {
                console.log('Check App Status ' + err);
                reject(err);
            });
    }));

};
/* Create an new Application into database and docker */
 exports.getDockerStatus  = function (req, res) {
     
      console.log('get an docker status');

      dockerService
            .listContainers()
            .then((result) => {
                responseService.send({
                    status: responseService.getCode().codes.OK,
                    data: result,
                }, res);

            })
            .catch((err) => {
                responseService.send({
                    status: responseService.getCode().codes.UNAUTHORIZED,
                    data: err,
                }, res);
            });

};

// Create an new docker app
exports.createNewApp = function (id, AppPort) {

    console.log('Create an new Application ');

    let finalResp = [];
    return new Promise(((resolve, reject) => {

        let name = helper.codeGenerator();

        let data = {
            in_user_id: id,
            in_port: AppPort,
            st_ip: process.env.NODE_ENV=="production" ? 'https://api.evivve.com':'http://localhost', // Todo : Change AWS Public IP
            active: '1',
            st_code: helper.appCodeGenerator(4),
            dt_added_at: moment().format('YYYY-MM-DD HH:mm:ss')
        };

        let params = {
            name: name,
            Image: 'node',
            AttachStdin: false,
            AttachStdout: true,
            AttachStderr: true,
            WorkingDir: "/var/www",
            ExposedPorts: {
                "3002/tcp": {}
            },
            Tty: true,
            StopTimeout: 3600,
            HostConfig: {
                Binds: [
                    "/home/ubuntu/app-master:/var/www" //TODO: We have to consider this ec2 address
                ],
                PortBindings: {
                    "3002/tcp": [
                        {
                            "HostPort": String(AppPort)
                        }
                    ]
                }
            },
            Tty: true,
            Cmd: ['bash', '-c', 'node ./server.js'],
        };

        dockerService
            .runContainer(params)
            .then((result) => {

                data.st_container = result;
                return dockerService.inspectContainer(result);

            }).then((result) => {
                console.log('New APP Container Status :' +result.State.State);
                if (result.State.Status == "running") {
                    console.log('Delete Previous App')
                    return application.deleteSpecificApp(id);
                } else {
                    return false;
                }
            }).then((result) => {
                console.log('Add into Database');
                console.log(data);
                return application.addApps(data);
            }).then((result) => {
                resolve(data);
            })
            .catch((err) => {
                console.log(err);
                reject(err);
            });
    }));
};

