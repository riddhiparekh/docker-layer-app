const ApplicationController = require('./application.controller');

class ApplicationRoute {
    constructor(app) {
        this.app = app;
        this.initRoutes();
    }
    initRoutes() {

        /* get an App info by code*/
        this.app.post(
            '/api/getAppInfo',
            (req, res, next) => {
                ApplicationController.getAppInfo(req, res, next);
            }
        );
        /* Restart an docker app*/
        this.app.post(
            '/api/restartApp',
            (req, res, next) => {
                ApplicationController.restartApp(req, res, next);
            }
        );
        /* get a containers status */
        this.app.get(
            '/api/docker/status',
            (req, res, next) => {
                ApplicationController.getDockerStatus(req, res, next);
            }
        );
        /* logout an application */
        this.app.post(
            '/api/logout',
            (req, res, next) => {
                ApplicationController.logoutApp(req, res, next);
            }
        );
    }
}

module.exports = ApplicationRoute;
