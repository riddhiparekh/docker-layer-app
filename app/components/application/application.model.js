const db = require('../../db/');
const appModel = require('../../models/appModel');
const logger = require('../../utils/logging');

exports.checkAppStatus = function (id) {
    return new Promise(((resolve, reject) => {
        // TODO: Add isActive checker and password checking
        const queryString = `SELECT * from ${appModel.tableName} 
        WHERE ${appModel.in_user_id}='${id}' `;
        db.query(queryString, (err, res) => {
            if (err) {
                logger.log('error', ` checkAppStatus - ${id} - ${err}`);
                reject(err);
            } else if (res.length === 0) {
                // user does not exist
                // eslint-disable-next-line prefer-promise-reject-errors
                resolve(null);
            } else {
                
                resolve(res[0]);
            }
        });
    }));
};

exports.getAppByCode = function (code) {
    return new Promise(((resolve, reject) => {

        const queryString = `SELECT * from ${appModel.tableName} 
        WHERE ${appModel.st_code}='${code}'`;
        db.query(queryString, (err, res) => {
            if (err) {
                logger.log('error', ` getAppByCode - ${code} - ${err}`);
                reject(err);
            } else if (res.length === 0) {
                // user does not exist
                // eslint-disable-next-line prefer-promise-reject-errors
                resolve(null);
            } else {
                
                resolve(res[0]);
            }
        });
    }));
};

exports.retriveApps = function () {
    return new Promise(((resolve, reject) => {
        // TODO: Add isActive checker and password checking
        const queryString = `SELECT * from ${appModel.tableName}`;
        db.query(queryString, (err, res) => {
            if (err) {
                logger.log('error', ` retriveApps - ${err}`);
                reject(err);
            } else {
                resolve(res);
            }
        });
    }));
};

exports.addApps = function (data) {
    return new Promise(((resolve, reject) => {
        // TODO: Add isActive checker and password checking
        const queryString = `INSERT INTO ${appModel.tableName} ( ${appModel.in_user_id} , ${appModel.in_port} , ${appModel.st_ip} , ${appModel.active}, ${appModel.st_code}, ${appModel.dt_added_at}, ${appModel.st_container} )
        VALUES ( '${data.in_user_id}' , '${data.in_port}', '${data.st_ip}', '${data.active}', '${data.st_code}', '${data.dt_added_at}', '${data.st_container}' )`;

        db.query(queryString, (err, res) => {
            if (err) {
                logger.log('error', ` addApps - ${email} - ${err}`);
                reject(err);
            } else if (res.length === 0) {
                // user does not exist
                // eslint-disable-next-line prefer-promise-reject-errors
                reject('App does not exist');
            } else {
                resolve(res);
            }
        });
    }));
};

exports.updateApp = function (data) {
    return new Promise(((resolve, reject) => {
        let updateData = '';
        Object.keys(data).forEach(ele => {
            if (ele != 'id' && ele != 'token') {
                if (typeof data[ele] === 'string') {
                    updateData = ' , ' + ele + " = '" + data[ele] + "'" + updateData;
                }
                else {
                    updateData = ' , ' + ele + ' = ' + data[ele] + updateData;
                }
            }
        });
        updateData = updateData.substr(2, updateData.length)
        const queryString = `UPDATE ${appModel.tableName} SET${updateData} where id = ${data.id}`;
        db.query(queryString, (err, res) => {
            if (err) {
                logger.log('error', ` updateApp - ${err}`);
                reject(err);
            } else {
                resolve(res);
            }
        });
    }));
};

exports.updateSpecificApp = function (id) {
    return new Promise(((resolve, reject) => {
        const queryString = `UPDATE ${appModel.tableName} SET ${appModel.active} = 0 where id = ${id}`;
        db.query(queryString, (err, res) => {
            if (err) {
                logger.log('error', ` updateSpecificApp - ${err}`);
                reject(err);
            } else {
                resolve(res);
            }
        });
    }));
};

exports.deleteSpecificApp = function (id) {
    return new Promise(((resolve, reject) => {
        const queryString = `DELETE FROM ${appModel.tableName} WHERE ${appModel.in_user_id}='${id}'`;
        db.query(queryString, (err, res) => {
            if (err) {
                logger.log('error', ` deleteSpecificApp - ${err}`);
                reject(err);
            } else {

                resolve(res);
            }
        });
    }));
};