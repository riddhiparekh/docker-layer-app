let {nextAvailable} = require('node-port-check');
const detect = require('detect-port');
const getPort = require('get-port');

exports.getAppPort =   function(port) {

  return new Promise(((resolve, reject) => {

	 nextAvailable(port).then((nextAvailablePort) => {
	   console.log('nextAvailable Port:'+nextAvailablePort);
	   		resolve(nextAvailablePort);
        });

    }));


}; 

 


exports.detectPort =   function(port) {

  return new Promise(((resolve, reject) => {

        detect(port, (err, _port) => {

              if (err) {
               
                  nextAvailable(port).then((nextAvailablePort) => {

                        resolve(nextAvailablePort);
                    });
              }
            console.log('Port '+port+'=='+_port);
              if (port == _port) {
                // Same port
                resolve(port);
              } else {
                // Generate

                      nextAvailable(port).then((nextAvailablePort) => {

                        resolve(nextAvailablePort);
                    });

              }
        });

    }));


}; 

exports.appCodeGenerator = function (len,charSet) {

   charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }

    return randomString;
} 

exports.codeGenerator = function () {
    var rand = function () {
        return Math.random().toString(36).substr(2);
    };

    var code = function () {
        return rand() + rand();
    };

    return code();
} 