const Otherusers = require('./otherusers/');
const Application = require('./application/');

class Components {
    constructor(app) {
        this.app = app;
        this.initModules();
    }

    initModules() {
        // TODO: Fetch components dynamically without creating objects
        // or use Awilix - ref - https://github.com/jeffijoe/awilix
        /* eslint-disable no-unused-vars */
        const appObj = new Application(this.app);
        /* eslint-enable no-unused-vars */
        const otherusersObj = new Otherusers(this.app);
    }
}

module.exports = Components;
