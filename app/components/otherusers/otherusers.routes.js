const otherusersController = require('./otherusers.controller');

class OtherusersRoute {
    constructor(app) {
        this.app = app;
        this.initRoutes();
    }
    initRoutes() {
        this.app.post(
            '/api/otherusers/login',
            (req, res, next) => {
                otherusersController.getAuthorization(req, res, next);
            }
        );

        this.app.post(
            '/api/otherusers/user',
            (req, res, next) => {
                otherusersController.postUser(req, res, next);
            }
        );

        this.app.get(
            '/api/otherusers/user',
            (req, res, next) => {
                otherusersController.getUsers(req, res, next);
            }
        );

        this.app.put(
            '/api/otherusers/user',
            (req, res, next) => {
                otherusersController.putUsers(req, res, next);
            }
        );

        this.app.put(
            '/api/otherusers/user/:id',
            (req, res, next) => {
                otherusersController.putSpecificUser(req, res, next);
            }
        );

        this.app.delete(
            '/api/otherusers/user/:id',
            (req, res, next) => {
                otherusersController.deleteSpecificUser(req, res, next);
            }
        );
    }
}

module.exports = OtherusersRoute;
