const db = require('../../db/');
const otherusersModel = require('../../models/otherusersModel');
const logger = require('../../utils/logging');

exports.checkCredentials = function (email, password) {
    return new Promise(((resolve, reject) => {
        // TODO: Add isActive checker and password checking
        const queryString = `SELECT ${otherusersModel.token},${otherusersModel.id} from ${otherusersModel.tableName} 
        WHERE ${otherusersModel.email}='${email}' and ${otherusersModel.password}='${password}' and ${otherusersModel.active}='1'`;
        db.query(queryString, (err, res) => {
            if (err) {
                logger.log('error', ` checkCredentials - ${email} - ${err}`);
                reject(err);
            } else if (res.length === 0) {
                // user does not exist
                // eslint-disable-next-line prefer-promise-reject-errors
                reject('Email does not exist');
            } else {
                resolve(res[0]);
            }
        });
    }));
};

exports.retriveUsers = function () {
    return new Promise(((resolve, reject) => {
        // TODO: Add isActive checker and password checking
        const queryString = `SELECT * from ${otherusersModel.tableName}`;
        db.query(queryString, (err, res) => {
            if (err) {
                logger.log('error', ` retriveUsers - ${err}`);
                reject(err);
            } else {
                resolve(res);
            }
        });
    }));
};

exports.addUsers = function (email, password, user_details, token) {
    return new Promise(((resolve, reject) => {
        // TODO: Add isActive checker and password checking
        const queryString = `INSERT INTO ${otherusersModel.tableName} ( ${otherusersModel.email} , ${otherusersModel.password} , ${otherusersModel.user_details} , ${otherusersModel.token} )
        VALUES ( '${email}' , '${password}', '${user_details}', '${token}' )`;
        db.query(queryString, (err, res) => {
            if (err) {
                logger.log('error', ` addUsers - ${email} - ${err}`);
                reject(err);
            } else if (res.length === 0) {
                // user does not exist
                // eslint-disable-next-line prefer-promise-reject-errors
                reject('Email does not exist');
            } else {
                resolve(res);
            }
        });
    }));
};

exports.updateUser = function (data) {
    return new Promise(((resolve, reject) => {
        let updateData = '';
        Object.keys(data).forEach(ele => {
            if (ele != 'id' && ele != 'token') {
                if (typeof data[ele] === 'string') {
                    updateData = ' , ' + ele + " = '" + data[ele] + "'" + updateData;
                }
                else {
                    updateData = ' , ' + ele + ' = ' + data[ele] + updateData;
                }
            }
        });
        updateData = updateData.substr(2, updateData.length)
        const queryString = `UPDATE ${otherusersModel.tableName} SET${updateData} where id = ${data.id}`;
        db.query(queryString, (err, res) => {
            if (err) {
                logger.log('error', ` updateUser - ${err}`);
                reject(err);
            } else {
                resolve(res);
            }
        });
    }));
};

exports.updateSpecificUser = function (id) {
    return new Promise(((resolve, reject) => {
        const queryString = `UPDATE ${otherusersModel.tableName} SET ${otherusersModel.active} = 0 where id = ${id}`;
        db.query(queryString, (err, res) => {
            if (err) {
                logger.log('error', ` updateSpecificUser - ${err}`);
                reject(err);
            } else {
                resolve(res);
            }
        });
    }));
};

exports.deleteSpecificUser = function (id) {
    return new Promise(((resolve, reject) => {
        const queryString = `DELETE FROM ${otherusersModel.tableName} WHERE id = ${id}`;
        db.query(queryString, (err, res) => {
            if (err) {
                logger.log('error', ` deleteSpecificUser - ${err}`);
                reject(err);
            } else {
                resolve(res);
            }
        });
    }));
};