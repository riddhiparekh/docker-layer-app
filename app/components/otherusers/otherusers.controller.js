/* eslint-disable no-underscore-dangle */   
const Otherusers = require('./otherusers.model');
const responseService = require('../../utils/ResponseService');
const logger = require('../../utils/logging');
const helper = require('./otherusers.helper');
// Application Controller
const ApplicationController = require('../application/application.controller');


// TODO: Add Validation for request parameters using Joi library
exports.getAuthorization = function (req, res) {
    const email = req.body.email;
    const password = req.body.password;
    let finalResp = {};

    if (email && password) {
        Otherusers
            .checkCredentials(email, password)
            .then(async (result) => {

                logger.log('info', `${email} loggedin successfully`);
                finalResp['token'] = result['token'];
                finalResp['id'] = result['id'];

                return await ApplicationController.checkAppStatus(result['id']);

            }).then(async (fromResolve) => {
                    console.log('Sending a request for creating a new docker app')
                return await ApplicationController.createNewApp(finalResp['id'], fromResolve['new_port']);
              
            }).then((fromResolve) => {

                console.log('New app created ' );
                console.log(fromResolve);

                if (fromResolve) {
                    

                    finalResp['in_port'] = fromResolve['in_port'];
                    finalResp['st_code'] = fromResolve['st_code'];
                    finalResp['st_ip'] = fromResolve['st_ip'];
                    console.log(finalResp);

                    responseService.send({
                        status: responseService.getCode().codes.OK,
                        data: finalResp,
                    }, res);

                } else {

                    responseService.send({
                        status: responseService.getCode().codes.FAILURE,
                        data: 'Internal Server Error',
                    }, res);

                }
            })
            .catch((err) => {
                console.log(err);
                logger.log('error', `${email} loggedin unsuccesful, wrong username/password error is - ${err} `);
                responseService.send({
                    status: responseService.getCode().codes.UNAUTHORIZED,
                    data: 'Wrong email and password',
                }, res);
            });
    } else {
        responseService.send({
            status: responseService.getCode().codes.BAD_REQUEST,
            data: 'Please provide email and password',
        }, res);
    }
};
/*
    Check IP/Port is assigned to that user
*/
exports.getUsers = function (req, res) {
    Otherusers
        .retriveUsers()
        .then((result) => {
            logger.log('info', `retriveUser successfully`);
            responseService.send({
                status: responseService.getCode().codes.OK,
                data: result,
            }, res);
        })
        .catch((err) => {
            logger.log('error', `retriveUser unsuccesful, error is - ${err} `);
            responseService.send({
                status: responseService.getCode().codes.UNAUTHORIZED,
                data: 'retrive user error',
            }, res);
        });
};

exports.postUser = function (req, res) {
    const email = req.body.email;
    const password = req.body.password;
    const user_details = req.body.user_details;
    if (email && password && user_details) {
        const token = helper.tokenGenerator();
        Otherusers
            .addUsers(email, password, user_details, token)
            .then((result) => {
                logger.log('info', `${email} insertion successfully`);
                responseService.send({
                    status: responseService.getCode().codes.OK,
                    data: result,
                }, res);
            })
            .catch((err) => {
                logger.log('error', `${email} insertion unsuccesful, error is - ${err} `);
                responseService.send({
                    status: responseService.getCode().codes.UNAUTHORIZED,
                    data: 'Insertion error! Please retry',
                }, res);
            });
    } else {
        responseService.send({
            status: responseService.getCode().codes.BAD_REQUEST,
            data: 'Please provide email and password and user_details',
        }, res);
    }
}

exports.putUsers = function (req, res) {
    if (req.body) {
        Otherusers
            .updateUser(req.body)
            .then((result) => {
                logger.log('info', `user updation successfully`);
                responseService.send({
                    status: responseService.getCode().codes.OK,
                    data: result,
                }, res);
            })
            .catch((err) => {
                logger.log('error', `user updation unsuccesful, error is - ${err} `);
                responseService.send({
                    status: responseService.getCode().codes.UNAUTHORIZED,
                    data: 'Updation error! Please retry',
                }, res);
            });
    } else {
        responseService.send({
            status: responseService.getCode().codes.BAD_REQUEST,
            data: 'Please provide email, password, user_details etc details!',
        }, res);
    }
}

exports.putSpecificUser = function (req, res) {
    if (req.params) {
        Otherusers
            .updateSpecificUser(req.params.id)
            .then((result) => {
                logger.log('info', `updateSpecificUser successfully`);
                responseService.send({
                    status: responseService.getCode().codes.OK,
                    data: result,
                }, res);
            })
            .catch((err) => {
                logger.log('error', `updateSpecificUser unsuccesful, error is - ${err} `);
                responseService.send({
                    status: responseService.getCode().codes.UNAUTHORIZED,
                    data: 'updateSpecificUser error! Please retry',
                }, res);
            });
    } else {
        responseService.send({
            status: responseService.getCode().codes.BAD_REQUEST,
            data: 'Please provide specific user details!',
        }, res);
    }
}

exports.deleteSpecificUser = function (req, res) {
    if (req.params) {
        Otherusers
            .deleteSpecificUser(req.params.id)
            .then((result) => {
                logger.log('info', `deleteSpecificUser successfully`);
                responseService.send({
                    status: responseService.getCode().codes.OK,
                    data: result,
                }, res);
            })
            .catch((err) => {
                logger.log('error', `deleteSpecificUser unsuccesful, error is - ${err} `);
                responseService.send({
                    status: responseService.getCode().codes.UNAUTHORIZED,
                    data: 'deleteSpecificUser error! Please retry',
                }, res);
            });
    } else {
        responseService.send({
            status: responseService.getCode().codes.BAD_REQUEST,
            data: 'Please provide specific user details!',
        }, res);
    }
}