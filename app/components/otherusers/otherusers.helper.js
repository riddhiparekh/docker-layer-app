exports.tokenGenerator = function () {
    var rand = function () {
        return Math.random().toString(36).substr(2);
    };

    var token = function () {
        return rand() + rand();
    };

    return token();
} 