const userModel = require('./userModel');
const calamityModel = require('./calamityModel');
const setupModel = require('./setupModel');

module.exports = {
    userModel,
    calamityModel,
    setupModel,
};
