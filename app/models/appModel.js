/*
	Application Info Model
   
*/
const application = {
    tableName: 'ev_applications',
    id: 'id',
    in_user_id: 'in_user_id',
    in_port: 'in_port',
    st_ip: 'st_ip',
    active: 'active',
    st_container: 'st_container',
    st_code: 'st_code',
    dt_added_at: 'dt_added_at',
};

module.exports = application;
